<!DOCTYPE html>
<html lang="es">
<head>
  <title>delimitadores en codigo php</title>
  <meta charset="utf-8"/>
  <link rel="stylesheet" href="css/tabe.css"/>
   <link rel="stylesheet" href="css/delimeters.css"/>
</head>
<body>
<header>
      <h1>Los delimitadores de codigo php</h1>
</header>
<section>
 <article>
     <div class="contenedor-tabs">
	     <?php
		   echo "<span class=\"diana\" id=\"una\"></span>\n";
		   echo "<div class=\"tab\">\n";
		   echo"<a href=\"una\" class=\"tab-e\">Estilo html</a>\n";
		   echo"<div class=\"first\">\n";
		   echo "<p class=\"xmltag\">\n";
		   echo"&lt;?php....?&lgt;.<br>\n";
		   echo"</p>\n";
		   echo"</div>\n";
		   echo"</div>\n";
		 ?>
		 <script language="php">
		 echo "<span class=\"diana\" id=\"dos\"></span>\n";
		   echo "<div class=\"tab\">\n";
		   echo"<a href=\"dos\" class=\"tab-e\">Estilo xml</a>\n";
		   echo"<div class=\"dos\">\n";
		   echo "<p class=\"htmltag\">\n";
		   echo"&lt;?script&gt....?&lt;/script&gt.<br>\n";
		   echo"</p>\n";
		   echo"</div>\n";
		   echo"</div>\n";
		 </script>
		 <?
		 echo "<span class=\"diana\" id=\"tres\"></span>\n";
		   echo "<div class=\"tab\">\n";
		   echo"<a href=\"tres\" class=\"tab-e\">Etiquetas cortas</a>\n";
		   echo"<div class=\"tres\">\n";
		   echo "<p class=\"shortag\">\n";
		   echo"&lt;....gt;.<br>\n";
		   echo"</p>\n";
		   echo"</div>\n";
		   echo"</div>\n";
		 ?>
		 <%
		 echo "<span class=\"diana\" id=\"cuatro\"></span>\n";
		   echo "<div class=\"tab\">\n";
		   echo"<a href=\"cuatro\" class=\"tab-e\">Estilo xml</a>\n";
		   echo"<div class=\"cuatro\">\n";
		   echo "<p class=\"asptag\">\n";
		   echo"&lt;....&gt;.<br>\n";
		   echo"</p>\n";
		   echo"</div>\n";
		   echo"</div>\n";
		 %>
	 </div>
 </article>
</section>
</body>
</html>